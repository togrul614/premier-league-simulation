<?php

namespace App\Console\Commands;

use App\Services\GenerateFixtureService;
use Illuminate\Console\Command;

class GenerateFixture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:fixture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Fixture for all teams';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private GenerateFixtureService $fixtureService
    )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->fixtureService->generateFixture();

        return 0;
    }
}
