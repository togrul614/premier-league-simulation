<?php
namespace App\Repositories;


use App\Interfaces\MatchesRepositoryInterface;
use App\Models\PlayedMatch;
use Illuminate\Database\Eloquent\Collection;

class MatchesRepository implements MatchesRepositoryInterface {

    public function get(): Collection
    {
        return PlayedMatch::query()->with(['home_team','away_team'])->orderByDesc('updated_at')->get();
    }

    public function getUnplayedGames(): Collection
    {
        return PlayedMatch::query()->with(['home_team','away_team'])->where('played',1)->get();
    }



    public function getResult($home_team, $away_team): array
    {
        $home_estimation = 1;
        $away_estimation = 0;
        if ($home_team->strength > $away_team->strength) {
            $home_estimation += 2;
        }else{
            $away_estimation += 2;
        }

        if ($home_estimation > $away_estimation){
            $result['home_score'] = rand(1,$home_estimation);
            $result['away_score'] = rand(0,$away_estimation);
        }else{
            $result['home_score'] = rand(0,$home_estimation);
            $result['away_score'] = rand(1,$away_estimation);
        }

        return $result;
    }

    public function checkTeamsPlayed($home_team, $away_team) : bool
    {
        return PlayedMatch::query()->where('home_team_id',$home_team->id)->where('away_team_id',$away_team->id)->exists();
    }

    public function getLastWeek() : int|null
    {
        $week = PlayedMatch::max('week');

        return !empty($week) ? $week : 0;
    }

    public function getByWeek(int $week) : Collection|null
    {
        return PlayedMatch::with('home_team','away_team')->where('week',$week)->get();
    }

    public function storePlayedMatch(int $home_team, int $away_team, $week, $home_score = 0, $away_score = 0){
        $playedMatch = new PlayedMatch();
        $playedMatch->home_team_id = $home_team;
        $playedMatch->away_team_id = $away_team;
        $playedMatch->home_team_goal = $home_score;
        $playedMatch->away_team_goal = $away_score;
        $playedMatch->week = $week;
        $playedMatch->save();

        return $playedMatch;
    }

}
