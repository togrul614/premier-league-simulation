<?php
namespace App\Repositories;


use App\Interfaces\TeamsRepositoryInterface;
use App\Models\PlayedMatch;
use App\Models\Team;
use Illuminate\Database\Eloquent\Collection;

class TeamsRepository implements TeamsRepositoryInterface {

    public function get(): Collection
    {
        return Team::query()->with(['home_matches','away_matches'])->get()->sortByDesc('points');
    }

    public function getResult($home_team, $away_team): array
    {
        $home_estimation = 1;
        $away_estimation = 0;
        if ($home_team->strength > $away_team->strength) {
            $home_estimation += 2;
        }else{
            $away_estimation += 2;
        }

        if ($home_estimation > $away_estimation){
            $result['home_score'] = rand(0,$home_estimation);
            $result['away_score'] = rand(0,$away_estimation);
        }else{
            $result['home_score'] = rand(0,$home_estimation);
            $result['away_score'] = rand(0,$away_estimation);
        }

        return $result;
    }

}
