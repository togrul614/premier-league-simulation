<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PlayedMatch extends Model
{
    use HasFactory;

    protected $table = 'played_matches';

    public function home_team() : BelongsTo
    {
        return $this->belongsTo(Team::class,'home_team_id');
    }

    public function away_team() : BelongsTo
    {
        return $this->belongsTo(Team::class,'away_team_id');
    }
}
