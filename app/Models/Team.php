<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';

    protected $fillable = ['played','win','draw','lost','goal_scored','goal_conceded'];

    public function getPointsAttribute() : int
    {
        return ($this->win * 3) + $this->draw;
    }

    public function getGoalDifference() : int
    {
        return $this->goals_scored - $this->goals_conceded;
    }

    public function home_matches() : HasMany
    {
        return $this->hasMany(PlayedMatch::class,'home_team_id');
    }

    public function away_matches() : HasMany
    {
        return $this->hasMany(PlayedMatch::class,'away_team_id');
    }
}
