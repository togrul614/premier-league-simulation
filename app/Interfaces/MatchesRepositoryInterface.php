<?php

namespace App\Interfaces;

interface MatchesRepositoryInterface
{
    public function get();
    public function getResult($home_team,$away_team);
    public function getUnplayedGames();
}
