<?php

namespace App\Interfaces;

interface TeamsRepositoryInterface
{
    public function get();
    public function getResult($home_team,$away_team);
}
