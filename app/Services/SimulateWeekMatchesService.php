<?php

namespace App\Services;

use App\Models\PlayedMatch;
use App\Repositories\MatchesRepository;
use App\Repositories\TeamsRepository;
use Exception;

class SimulateWeekMatchesService {

    public int $week;

    public function __construct(
        private  MatchesRepository $matchesRepository,
        private  TeamsRepository $teamsRepository,
    )
    {
    }

    public function simulateWeekMatches() : void
    {
        $teams = $this->teamsRepository->get();
        $weeks = $this->getWeekCount($teams->count());

        $fixtures = $this->generateHomeAwayFixtures($teams->pluck('id')->toArray());

        // Display fixtures
        foreach ($fixtures as $round => $roundFixtures) {
            foreach ($roundFixtures as $match) {
                $this->matchesRepository->storePlayedMatch($match['home_team'],$match['away_team'],$round);
            }
        }

    }
}