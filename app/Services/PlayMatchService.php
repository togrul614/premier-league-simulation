<?php

namespace App\Services;

use App\Models\PlayedMatch;
use App\Repositories\MatchesRepository;
use App\Repositories\TeamsRepository;
use Exception;

class PlayMatchService {

    public int $week;

    public function __construct(
        private  MatchesRepository $matchesRepository,
        private  TeamsRepository $teamsRepository,
    )
    {
    }

//    public function simulateAllMatches()
//    {
//        // Get all teams
//        $teams = $this->teamsRepository->get();
//
//        $this->week = 0;
//
//        for ($i = 0; $i < $teams->count();  $i++){
//            $this->week++;
//            for($j = $i+1; $j < $teams->count(); $j++){
//                $this->playMatch($teams[$i], $teams[$j]);
//                $this->playMatch($teams[$j], $teams[$i]);
//            }
//        }
//    }

    public function simulateWeekMatches() : void
    {
        $teams = $this->teamsRepository->get();
        $weeks = getWeekCount($teams->count());

        $fixtures = $this->generateHomeAwayFixtures($teams->pluck('id')->toArray());

        // Display fixtures
        foreach ($fixtures as $round => $roundFixtures) {
            foreach ($roundFixtures as $match) {
                $this->matchesRepository->storePlayedMatch($match['home_team'],$match['away_team'],$round);
            }
        }

    }

    private function playMatch($homeTeam, $awayTeam): void
    {
            // Simulate a match between two teams
            $results = $this->teamsRepository->getResult($homeTeam, $awayTeam);

            // Update team statistics
            $homeTeam->goals_scored += $results['home_score'];
            $homeTeam->goals_conceded += $results['away_score'];
            $awayTeam->goals_scored += $results['away_score'];
            $awayTeam->goals_conceded += $results['home_score'];

            // Update points based on match result (3 points for a win, 1 for a draw)
            if ($results['home_score'] > $results['away_score']) {
                $homeTeam->win += 1;
                $awayTeam->lost += 1;
            } elseif ($results['home_score'] === $results['away_score']) {
                $homeTeam->draw += 1;
                $awayTeam->draw += 1;
            } else {
                $homeTeam->lost += 1;
                $awayTeam->win += 1;
            }

            $homeTeam->save();
            $awayTeam->save();

            $playedMatch = new PlayedMatch();
            $playedMatch->home_team_id = $homeTeam->id;
            $playedMatch->away_team_id = $awayTeam->id;
            $playedMatch->home_team_goal = $results['home_score'];
            $playedMatch->away_team_goal = $results['away_score'];
            $playedMatch->week = $this->week;
            $playedMatch->save();
    }

    public function getWeekCount(int $teamsCount) : int
    {
        return ($teamsCount - 1) * 2;
    }
}