<?php

namespace App\Services;

use App\Models\PlayedMatch;
use App\Repositories\MatchesRepository;
use App\Repositories\TeamsRepository;
use Exception;

class GenerateFixtureService {

    public int $week;

    public function __construct(
        private  MatchesRepository $matchesRepository,
        private  TeamsRepository $teamsRepository,
    )
    {
    }


    public function generateFixture() : void
    {
        $teams = $this->teamsRepository->get();

        $roundCount = getWeekCount($teams->count());

        $lastWeek  =  $this->matchesRepository->getLastWeek();

        if ($roundCount == $lastWeek){
            return;
        }

        $fixtures = $this->generateHomeAwayFixtures($teams->pluck('id')->toArray());

        // Display fixtures
        foreach ($fixtures as $round => $roundFixtures) {
            foreach ($roundFixtures as $match) {
                $this->matchesRepository->storePlayedMatch($match['home_team'],$match['away_team'],$round);
            }
        }
    }

    function generateHomeAwayFixtures(array $teams): array
    {
        $totalTeams = count($teams);
        $fixtures = [];

        $roundCount = $this->getRoundCount($totalTeams);

        for ($round = 1; $round < $roundCount+1; $round++) {
            $roundFixtures = [];
            for ($i = 0; $i < $totalTeams / 2; $i++) {
                $homeTeamIndex = ($round + $i) % ($totalTeams - 1);
                $awayTeamIndex = ($round + $totalTeams - 1 - $i) % ($totalTeams - 1);

                if ($i === 0) {
                    // Fix the first team in the fixture
                    $awayTeamIndex = $totalTeams - 1;
                }

                $homeTeam = $teams[$homeTeamIndex];
                $awayTeam = $teams[$awayTeamIndex];

                $roundFixtures[] = [
                    'home_team' => $homeTeam,
                    'away_team' => $awayTeam,
                ];
            }

            $fixtures[$round] = $roundFixtures;
        }

        return $fixtures;
    }

    function getRoundCount($totalTeams) : int
    {
       return $totalTeams * 2 - 2;
    }
}