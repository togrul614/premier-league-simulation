<?php

if (!function_exists('getWeekCount')){
    function getWeekCount($teamsCount) : int
    {
        return ($teamsCount - 1) * 2;
    }
}