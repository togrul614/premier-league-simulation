<?php

namespace App\Http\Controllers;

use App\Repositories\MatchesRepository;
use App\Repositories\TeamsRepository;
use App\Services\PlayMatchService;
use App\Services\SimulateAllMatchesService;

class LeagueController extends Controller
{
    public function __construct(
        private  TeamsRepository $teamsRepository,
        private  MatchesRepository $matchesRepository,
        private  SimulateAllMatchesService $allMatchesService,
    )
    {
    }

    public function index()
    {
        $teams = $this->teamsRepository->get();

        return view('index', compact('teams'));
    }

    public function simulateAllMatches()
    {
        $this->allMatchesService->simulateAllMatches();


    }

}
