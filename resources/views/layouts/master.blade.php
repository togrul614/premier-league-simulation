<!DOCTYPE html>
<html lang="en">
<head>
    <title>League Simulation</title>
    @include('includes.styles')
</head>
<body>
@include('includes.header')
 @yield('content')

@include('includes.scripts')
</body>
</html>
