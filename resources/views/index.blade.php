@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>League Table</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Teams</th>
                            <th scope="col">Points</th>
                            <th scope="col">P</th>
                            <th scope="col">W</th>
                            <th scope="col">D</th>
                            <th scope="col">L</th>
                            <th scope="col">GD</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($teams as $team)
                            <tr>
                                <td>{{$team->name}}</td>
                                <td>{{$team->points}}</td>
                                <td>{{$team->played}}</td>
                                <td>{{$team->win}}</td>
                                <td>{{$team->draw}}</td>
                                <td>{{$team->lost}}</td>
                                <td>{{$team->getGoalDifference()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <button type="button" id="all_matches" class="btn btn-primary">Play All Matches</button>
                <button type="button" id="week_matches" class="btn btn-success">Play Next Week</button>
            </div>
        </div>
    </div>
@stop
